package com.conpany.project;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Article {
    private Integer id;
    private String  title;
    private String  content;

}
