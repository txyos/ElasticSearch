package com.conpany.project;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.xml.internal.bind.v2.TODO;
import org.elasticsearch.action.admin.cluster.snapshots.status.TransportNodesSnapshotsStatus;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Requests;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.xcontent.XContent;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHitField;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.junit.Test;
import org.springframework.web.bind.annotation.PostMapping;

import javax.naming.directory.SearchResult;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.SQLOutput;
import java.util.Iterator;

public class EsTest {

    @Test
    public void es_create_index() throws Exception {
        //创建client连接对象
        Settings build = Settings.builder().put("cluster.name", "my‐elasticsearch").build();
        //获取索引连接客户端
       TransportClient client = new PreBuiltTransportClient(build)
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("127.0.0.1"),9300));
       client.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("127.0.0.1"),9301));
       client.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("127.0.0.1"),9302));
       client.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("127.0.0.1"),9303));
        //创建索引--zl
        client.admin().indices().prepareCreate("zl-1").get();
        //释放资源
        client.close();
    }
    @Test
    public void test_mapping_XContentBuilder() throws Exception {
        //创建client连接
        Settings build = Settings.builder().put("cluster.name", "my‐elasticsearch").build();
        //获取索引连接客户端
        TransportClient client = new PreBuiltTransportClient(build)
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("127.0.0.1"),9300));
        //添加映射
        XContentBuilder builder = XContentFactory.jsonBuilder();
        //添加标题,属性,
        builder.startObject()
                .startObject("article")//类型
                .startObject("properties")//字段
                .startObject("id")
                //添加域
                .field("type","long").field("store","yes").endObject()
                //下一个属性
                .startObject("tltle")
                //添加域
                .field("type","string").field("store","yes").field("analyzer","ik_smart").endObject()
                .startObject("content")
                //添加域
                .field("type","string").field("store","yes").field("analyzer","ik_smart").endObject()
                .endObject()
                .endObject()
                .endObject();
        //创建映射
        PutMappingRequest zl = Requests.putMappingRequest("zl-1").type("article").source(builder);
        //添加映射
        /**
         * putMapping()
         * 创建映射关系
         * 属性--类型
         */
        client.admin().indices().putMapping(zl).get();
        //释放资源
        client.close();

    }




/*    @Test
    public void test_mapping_Entity() throws UnknownHostException, JsonProcessingException {
        //创建client连接
        Settings build = Settings.builder().put("cluster.name", "my‐elasticsearch").build();
        //获取索引连接客户端
        TransportClient client = new PreBuiltTransportClient(build)
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("127.0.0.1"),9300));

    }*/




    @Test
    public void test_document_XContentBuilder() throws IOException {
        //创建client连接
        Settings build = Settings.builder().put("cluster.name", "my‐elasticsearch").build();
        //获取索引连接客户端
        TransportClient client = new PreBuiltTransportClient(build)
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("127.0.0.1"),9300));
        XContentBuilder builder = XContentFactory.jsonBuilder();
        builder.startObject()
                .field("id",1)
                .field("title","涨了李的测试标题")
                .field("content","张磊的测试内容")
                .endObject();
        //添加文档内容
        client.prepareIndex("zl","article","1").setSource(builder).get();
        client.close();
    }
    //添加文档
    @Test
    public void test_document_entity() throws UnknownHostException, JsonProcessingException {
        //创建client连接
        Settings build = Settings.builder().put("cluster.name", "my‐elasticsearch").build();
        //获取索引连接客户端
        TransportClient client = new PreBuiltTransportClient(build)
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("127.0.0.1"),9300));
        //创建文档通过实体转json
        //描述json数据
        for (int i=100;i>=0;i--){
            Article article = new Article(i,i+"-zl-1的测试标题","zl-1的测试内容");
            ObjectMapper objectMapper = new ObjectMapper();
            //建立文档
            client.prepareIndex("zl-1","article",article.getId().toString()).setSource(objectMapper.writeValueAsString(article).getBytes(), XContentType.JSON).get();
        }
        //释放资源
        client.close();
    }


    /**
     * 查询文档
     * 查询文档分页
     * 查询结果高亮
     */

    //关键词查询
    @Test
    public void test_TermQuery() throws UnknownHostException {
        //创建client连接
        Settings build = Settings.builder().put("cluster.name", "my‐elasticsearch").build();
        //获取索引连接客户端
        TransportClient client = new PreBuiltTransportClient(build)
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("127.0.0.1"),9300));
        //设置搜索条件
        SearchResponse searchResponse = client.prepareSearch("zl")
                .setTypes("article")
                .setQuery(QueryBuilders.termQuery("content","测试的")).get();
        //遍历搜索结果数据
        SearchHits hits = searchResponse.getHits();//获取命中次数,查询结果有多少对象
        System.out.println(hits.getTotalHits());
        Iterator<SearchHit> iterator = hits.iterator();
        while (iterator.hasNext()){
            SearchHit searchHit = iterator.next();//每个查询对象
            System.out.println(searchHit.getSourceAsString());//获取字符串格式打印
            System.out.println(searchHit.getSource().get("title"));//获取标题
        }
        client.close();
    }

    //字符串查询

    /**
     * 字符串查询相比于关键词查询范围更广一点
     * @throws UnknownHostException
     */
    @Test
    public void test_StingQuery() throws UnknownHostException {
        //创建client连接
        Settings build = Settings.builder().put("cluster.name", "my‐elasticsearch").build();
        //获取索引连接客户端
        TransportClient client = new PreBuiltTransportClient(build)
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("127.0.0.1"),9300));
        //设置搜索条件
        SearchResponse searchResponse = client.prepareSearch("zl")
                .setTypes("article")
                .setQuery(QueryBuilders.queryStringQuery("测试的")).get();//字符串查询
        //遍历搜索结果数据
        SearchHits hits = searchResponse.getHits();//获取命中次数,查询结果有多少对象
        System.out.println(hits.getTotalHits());
        Iterator<SearchHit> iterator = hits.iterator();
        while (iterator.hasNext()){
            SearchHit searchHit = iterator.next();//每个查询对象
            System.out.println(searchHit.getSourceAsString());//获取字符串格式打印
            System.out.println(searchHit.getSource().get("title"));//获取标题
        }
        client.close();
    }

    //通过文档id查询
    @Test
    public void test_IDQuery() throws UnknownHostException {
        //创建client连接
        Settings build = Settings.builder().put("cluster.name", "my‐elasticsearch").build();
        //获取索引连接客户端
        TransportClient client = new PreBuiltTransportClient(build)
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("127.0.0.1"),9300));
        //设置搜索条件
        SearchResponse searchResponse = client.prepareSearch("zl")
                .setTypes("article")
                .setQuery(QueryBuilders.idsQuery().addIds("1")).get();//字符串查询
        //遍历搜索结果数据
        SearchHits hits = searchResponse.getHits();//获取命中次数,查询结果有多少对象
        System.out.println(hits.getTotalHits());
        Iterator<SearchHit> iterator = hits.iterator();
        while (iterator.hasNext()){
            SearchHit searchHit = iterator.next();//每个查询对象
            System.out.println(searchHit.getSourceAsString());//获取字符串格式打印
            System.out.println(searchHit.getSource().get("title"));//获取标题
        }
        client.close();
    }

//    分页查询

    @Test
    public void test_pageQuery() throws UnknownHostException {
        //创建client连接
        Settings build = Settings.builder().put("cluster.name", "my‐elasticsearch").build();
        //获取索引连接客户端
        TransportClient client = new PreBuiltTransportClient(build)
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("127.0.0.1"),9300));
        //设置搜索条件
//        TODO 默认查询10条
        /**
         * setForm():从第几条开始检索
         * setSize():每页显示的记录数
         *
         */
        //搜索数据
        //默认每页10条记录
        SearchRequestBuilder searchRequestBuilder = client.prepareSearch("zl-1").setTypes("article").setQuery(QueryBuilders.matchAllQuery());
        searchRequestBuilder.setFrom(0).setSize(5);
        SearchResponse searchResponse = searchRequestBuilder.get();

        //遍历搜索结果数据
        SearchHits hits = searchResponse.getHits();//获取命中次数,查询结果有多少对象
        System.out.println(hits.getTotalHits());
        Iterator<SearchHit> iterator = hits.iterator();
        while (iterator.hasNext()){
            SearchHit searchHit = iterator.next();//每个查询对象
            System.out.println(searchHit.getSourceAsString());//获取字符串格式打印
            System.out.println(searchHit.getSource().get("title"));//获取标题
        }
        client.close();
    }

    /**
     * 获取数据高亮显示
     * @throws UnknownHostException
     */
    @Test
    public void test_HightLightQuery() throws UnknownHostException {
        //创建client连接
        Settings build = Settings.builder().put("cluster.name", "my‐elasticsearch").build();
        //获取索引连接客户端
        TransportClient client = new PreBuiltTransportClient(build)
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("127.0.0.1"),9300));


        //设置搜索条件
//        TODO 默认查询10条
        /**
         * setForm():从第几条开始检索
         * setSize():每页显示的记录数
         *
         */
        //搜索数据
        //默认每页10条记录
        SearchRequestBuilder searchRequestBuilder = client.prepareSearch("zl-1")
                .setTypes("article").setQuery(QueryBuilders.termQuery("title" ,"测"));
        searchRequestBuilder.setFrom(0).setSize(5);

        //设置高亮
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.preTags("<font style='color:red'>");
        highlightBuilder.postTags("</font>");
        highlightBuilder.field("title");
        searchRequestBuilder.highlighter(highlightBuilder);

        SearchResponse searchResponse = searchRequestBuilder.get();

        //遍历搜索结果数据
        SearchHits hits = searchResponse.getHits();//获取命中次数,查询结果有多少对象
        System.out.println(hits.getTotalHits());
        Iterator<SearchHit> iterator = hits.iterator();
        while (iterator.hasNext()){
            SearchHit searchHit = iterator.next();//每个查询对象
            System.out.println(searchHit.getSourceAsString());//获取字符串格式打印
            System.out.println(searchHit.getSource().get("title"));//获取标题
            System.out.println(searchHit.getHighlightFields());//设置map方式高亮展示
            //遍历高亮部分
            Text[] titles = searchHit.getHighlightFields().get("title").getFragments();
            for (Text title : titles) {
                System.out.println(title);
            }
        }
        client.close();
    }



}
